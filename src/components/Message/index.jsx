import style from './message.module.scss';

export function Message({log}) {

	return (
		<div className={style.body}>
			<span className={style.item}>{`Button №${log.button}`}</span>
			<span className={style.item}>{log.log}</span>
			<span className={style.item}>-</span>
			<span className={style.item}>{log.click}</span>
			<span className={style.item}>{`(${log.passed})`}</span>
		</div>
	)
}
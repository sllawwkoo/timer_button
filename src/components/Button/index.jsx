export function Button({text, btnStyle, clickBtn}) {
	
	return (
		<button
			className={btnStyle}
			type="button"
			onClick={clickBtn}
		>
			{text}
		</button>
	)
}
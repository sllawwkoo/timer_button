import style from './log.module.scss';
import { Message } from '../Message';
import { useSelector } from 'react-redux';

export function Log() {
	const { logs } = useSelector((state) => state.logs);

	return (
		<div className={style.body}>
			{ logs.map((log) => (
				<Message 
					key={log.id}
					log={log}
				/>
			)
				
			)}
		</div>
	)
}
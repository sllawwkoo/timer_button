import { useState ,useEffect, useMemo } from 'react';
import { Button } from '../Button';
import { Log } from '../Log';
import style from './page.module.scss';
import { useDispatch, useSelector } from 'react-redux';
import { updateLogs } from '../../redux/actions/logs';
import { v4 as uuidv4 } from 'uuid';
import { subtractSeconds } from '../../utils/helpers/helpers';

export function Page() {
	const dispatch = useDispatch();
	const { logs } = useSelector((state) => state.logs);

	const [logEntryQueue, setLogEntryQueue] = useState([]);
	const [isProcessing, setIsProcessing] = useState(false);

	const buttons = useMemo(() => [
		{ text: '1 sec', delay: 1000 },
		{ text: '2 sec', delay: 2000 },
		{ text: '3 sec', delay: 3000 },
	], []);

		const handleButtonClick = (index) => {
	
		const logEntry = {
			id: uuidv4(),
			buttonIndex: index,
			button: index + 1,
			log: '',
			click: new Date().toLocaleTimeString(),
			passed: '',
		};

		setLogEntryQueue((prevQueue) => [...prevQueue, logEntry]);
	};

const delay = (timeout) => {
	return new Promise((resolve) => setTimeout(resolve, timeout));
};

useEffect(() => {
	const processLogQueue = async () => {
		if (isProcessing || logEntryQueue.length === 0) return;

		setIsProcessing(true);

		const logEntry = logEntryQueue[0];
		const button = buttons[logEntry.buttonIndex];

		await delay(button.delay);

		const updatedLogEntry = { ...logEntry };
		updatedLogEntry.log = new Date().toLocaleTimeString();
		const passed = subtractSeconds(updatedLogEntry.log, updatedLogEntry.click);
		updatedLogEntry.passed = `${passed.toFixed(0)} sec`;

		const updatedLog = [...logs, updatedLogEntry];
		dispatch(updateLogs(updatedLog));

		setLogEntryQueue((prevQueue) => prevQueue.slice(1));

		setIsProcessing(false);
	};

	processLogQueue();
}, [logEntryQueue, buttons, logs, dispatch, isProcessing]);

	const handleClearButtonClick = () => {
		dispatch(updateLogs([]))
	};
	
	return (
		<div className={style.wrapper}>
			<div className={style.container}>
				<div className={style.btnBody}>
					{buttons.map((button, index) => (
						<Button
							key={index}
							text={button.text}
							btnStyle={`${style.btn} ${style['btn' + (index + 1)]}`}
							clickBtn={() => handleButtonClick(index)}
						/>
					))}
				</div>
				<div className={style.clearBody}>
					<Button 
						text={'Clear'}
						btnStyle={style.btnClear}
						clickBtn={handleClearButtonClick}
					/>
				</div>
				<div className={style.logBody}>
					<h2 className={style.logTitle}>«Log»</h2>
					<Log />
				</div>
			</div>
		</div>
	)
}
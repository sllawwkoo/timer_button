import { logsTypes } from "../types";

export function setLogs(logs){
	return {
		type: logsTypes.SET_LOGS,
		playload: logs
	}
};

export function updateLogs(logs) {
	return function (dispatch) {
		localStorage.setItem("logsLS", JSON.stringify(logs));
		dispatch(setLogs(logs))
	};
}
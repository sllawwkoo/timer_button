import { combineReducers } from "redux";

// Reducers

import { logsReducer as logs } from "./reducers/logs";

export const rootReducer = combineReducers({ logs });

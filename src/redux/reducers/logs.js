import { logsTypes } from "../types";
import { getDataFromLS } from "../../utils/helpers/helpers";

const initialState = {
	logs: getDataFromLS('logsLS')
}

export function logsReducer(state = initialState, action) {
	switch (action.type) {
		case logsTypes.SET_LOGS:
			return {
				...state,
				logs:action.playload,
			}
			default:
				return state
	}
}
export function getDataFromLS(key) {
	const lsData = localStorage.getItem(key);
	if (!lsData) return [];
	try {
		const value = JSON.parse(lsData);
		return value;
	}
	catch (e) { 
		return [];
	}
};

export function subtractSeconds(time1, time2) {
	const [hours1, minutes1, seconds1] = time1.split(':').map(Number);
	const [hours2, minutes2, seconds2] = time2.split(':').map(Number);

	const totalSeconds1 = hours1 * 3600 + minutes1 * 60 + seconds1;
	const totalSeconds2 = hours2 * 3600 + minutes2 * 60 + seconds2;

	const differenceInSeconds = totalSeconds1 - totalSeconds2;

	return differenceInSeconds;
}
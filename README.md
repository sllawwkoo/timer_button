## Test Timer Button

 #### Звдання:
* Зверху розташовані три кнопки. По кліку на кожну повинен запускатися таймер із затримкою вказаною на кнопці. 
* Таймер наступної нажатої кнопки запускається тільки після завершення попередньої запущеної кнопки. Інтерфейс не блокується тобто запустив декілька таймерів підряд вони відпрацьовують послідовно один за одним. 
* Після завершення таймера в Лог додається запис наступного формату: __Button№{button}: {log} - {click} ({passed} sec)__. Де __button__ - номер кнопки(кнопка "2 sec" це №2), __log__ - время вивода в лог, __click__ - время натискання кнопки, __passed__ - різниця між натисканням кнопки та виводом в лог. 
* Записи в лог слідують в порядку натискання на кнопки.
* Лог збарігається в __Redux__.
* В інтерфейсі є кнопка __"Clear"__. При натисканні на неї додаток стає по дефолту.